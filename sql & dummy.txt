CREATE TABLE provinces (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
) ENGINE=InnoDB;

CREATE TABLE regencies (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    population INT NOT NULL,
    province_id INT NOT NULL,
    FOREIGN KEY (province_id) REFERENCES provinces(id)
) ENGINE=InnoDB;

INSERT INTO provinces (name) VALUES ('Province 1'), ('Province 2');

INSERT INTO regencies (name, population, province_id) VALUES
    ('Regency 1', 100000, 1),
    ('Regency 2', 150000, 1),
    ('Regency 3', 80000, 2);
