<!DOCTYPE html>
<html>

<head>
    <title>Data Provinsi</title>
</head>

<body>
    <h1>Data Provinsi</h1>
    <form method="post" action="/provinces">
        @csrf
        <input type="text" name="name" placeholder="Nama Provinsi" required>
        <button type="submit">Tambah</button>
    </form>
    <table>
        <tr>
            <th>ID</th>
            <th>Nama Provinsi</th>
            <th>Aksi</th>
        </tr>
        @foreach($provinces as $province)
        <tr>
            <td>{{ $province->id }}</td>
            <td>{{ $province->name }}</td>
            <td>
                <form method="post" action="/provinces/{{ $province->id }}">
                    @method('DELETE')
                    @csrf
                    <button type="submit">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</body>

</html>