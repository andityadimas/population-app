<!DOCTYPE html>
<html>

<head>
    <title>Data Kabupaten</title>
</head>

<body>
    <h1>Data Kabupaten</h1>
    <form method="post" action="/regencies">
        @csrf
        <input type="text" name="name" placeholder="Nama Kabupaten" required>
        <input type="number" name="population" placeholder="Jumlah Penduduk" required>
        <select name="province_id">
            @foreach($provinces as $province)
            <option value="{{ $province->id }}">{{ $province->name }}</option>
            @endforeach
        </select>
        <button type="submit">Tambah</button>
    </form>
    <table>
        <tr>
            <th>ID</th>
            <th>Nama Kabupaten</th>
            <th>Jumlah Penduduk</th>
            <th>Provinsi</th>
            <th>Action</th>
        </tr>
        @foreach($regencies as $regency)
        <tr>
            <td>{{ $regency->id }}</td>
            <td>{{ $regency->name }}</td>
            <td>{{ $regency->population }}</td>
            <td>{{ $regency->province->name }}</td>
            <td>
                <form method="post" action="/regencies/{{ $regency->id }}">
                    @method('PUT')
                    @csrf
                    <button type="submit">Edit</button>
                </form>
                <form method="post" action="/regencies/{{ $regency->id }}">
                    @method('DELETE')
                    @csrf
                    <button type="submit">Hapus</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</body>

</html>