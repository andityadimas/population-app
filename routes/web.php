<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
use App\Http\Controllers\PopulationController;

Route::get('/provinces', [PopulationController::class, 'showProvinces'])->name('provinces');
Route::post('/provinces', [PopulationController::class, 'addProvince']);
Route::put('/provinces/{id}', [PopulationController::class, 'editProvince']);
Route::delete('/provinces/{id}', [PopulationController::class, 'deleteProvince']);

Route::get('/regencies', [PopulationController::class, 'showRegencies'])->name('regencies');
Route::post('/regencies', [PopulationController::class, 'addRegency']);
Route::put('/regencies/{id}', [PopulationController::class, 'editRegency']);
Route::delete('/regencies/{id}', [PopulationController::class, 'deleteRegency']);


Route::get('/', function () {
    return view('welcome');
});
