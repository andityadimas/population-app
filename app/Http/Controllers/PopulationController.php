<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;

class PopulationController extends Controller
{
    public function showProvinces()
    {
        $provinces = Province::all();
        return view('provinces', compact('provinces'));
    }

    public function addProvince(Request $request)
    {
        Province::create($request->all());
        return redirect()->route('provinces');
    }

    public function editProvince(Request $request, $id)
    {
        $province = Province::findOrFail($id);
        $province->update($request->all());
        return redirect()->route('provinces');
    }

    public function deleteProvince($id)
    {
        Province::destroy($id);
        return redirect()->route('provinces');
    }

    public function showRegencies()
    {
        $regencies = Regency::with('province')->get();
        $provinces = Province::all();
        return view('regencies', compact('regencies', 'provinces'));
    }

    public function addRegency(Request $request)
    {
        Regency::create($request->all());
        return redirect()->route('regencies');
    }

    public function editRegency(Request $request, $id)
    {
        $regency = Regency::findOrFail($id);
        $regency->update($request->all());
        return redirect()->route('regencies');
    }

    public function deleteRegency($id)
    {
        Regency::destroy($id);
        return redirect()->route('regencies');
    }
}
