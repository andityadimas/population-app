<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Regency extends Model
{
    protected $fillable = ['name', 'province_id', 'population'];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
